FROM scratch

MAINTAINER skesterson@k-labs.com

COPY dockerSpy /dockerSpy

EXPOSE 8080

ENTRYPOINT [ "/dockerSpy" ]
