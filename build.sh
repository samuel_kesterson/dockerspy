#!/bin/bash

DOCKER_REPO=sk/dockerspy
VERSION=$(grep 'const VERSION' dockerSpy.go | cut -d ' ' -f 4 | sed -re 's@"@@g')

CGO_ENABLED=0 GOOS=linux go build -a -tags netgo -ldflags '-w' dockerSpy.go

DOCKER_AUTH=$(aws ecr get-login --region us-west-2)
DOCKER_REGISTRY=$(echo ${DOCKER_AUTH} | cut -d ' ' -f 9 | sed -re 's@https://(.*)$@\1@g')

${DOCKER_AUTH}

docker build -t ${DOCKER_REGISTRY}/${DOCKER_REPO}:${VERSION} .
docker push ${DOCKER_REGISTRY}/${DOCKER_REPO}:${VERSION} 

