/// dockerSpy.go
/// A silly little idea that will almost certainly get out of control
/// skesterson@k-labs.com

package main

import (
    "os"
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
)

const VERSION = "0.0.2"

func FetchAWSAttribute(url string) (b []byte) {
    resp, err := http.Get(url)
    if err != nil {
        fmt.Fprintf(os.Stderr, "fetch: %v\n", err)
        return []byte("unavailable")
    }
    b, err = ioutil.ReadAll(resp.Body)
    if err != nil {
        fmt.Fprintf(os.Stderr, "fetch: reading %s: %v\n", url, err)
        return []byte("unavailable")
    }
    return b
}

func main() {
    http.HandleFunc("/", handler)
    log.Fatal(http.ListenAndServe("0.0.0.0:8080", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "# dockerSpy %s\n#\n", VERSION)
    fmt.Fprintf(w, "# This utility prints out useful information about a Docker host.\n")
    hostname, _ := os.Hostname()
    fmt.Fprintf(w, "# Hostname: %s\n", hostname)
    fmt.Fprintf(w, "# EC2 Internal Hostname:\n%s\n",
                    FetchAWSAttribute("http://169.254.169.254/latest/meta-data/hostname"))
    fmt.Fprintf(w, "# EC2 Internal IPv4:\n%s\n",
                    FetchAWSAttribute("http://169.254.169.254/latest/meta-data/local-ipv4"))
    kernel_version, _ := ioutil.ReadFile("/proc/version")
    fmt.Fprintf(w, "# Kernel version: %s\n", kernel_version)
    meminfo, _ := ioutil.ReadFile("/proc/meminfo")
    fmt.Fprintf(w, "# RAM:\n%s\n", meminfo)
    cpu, _ := ioutil.ReadFile("/proc/cpuinfo")
    fmt.Fprintf(w, "# CPU:\n%s\n", cpu)
    /// Dump environment
    fmt.Fprintf(w, "\n# Environment variables\n")
    for _, env_var := range os.Environ() {
        fmt.Fprintf(w, "%s\n", env_var)
    }
}
